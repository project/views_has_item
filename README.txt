Views Has Item
==============

This simple module adds a new filter for each field created on the site. 
Specifically this filter see if the entity has at least one element or not 
in a particular field.

For example you could create a view with an exposed filter to see which 
contents have at least one term in the Tags field.

After you enable the module , you can find a new options in the filter criteria 
for each field. These end with ":has_item"
